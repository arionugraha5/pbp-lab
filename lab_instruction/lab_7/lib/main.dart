import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Belajar Form Flutter",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  String _nama = "";
  String _hobi = "";
  String gabungan = "";
  bool adaNama = false;
  bool adaHobi = false;

  void updateNama(String nama) {
    setState(() {
      _nama = nama;
      adaNama = true;
    });
  }

  void updateHobi(String hobi) {
    setState(() {
      _hobi = hobi;
      adaHobi = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Form"),
          backgroundColor: Colors.black,
        ),
        body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(20.0),
              child: Column(children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      labelText: "Name",
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    onChanged: (String value) {
                      updateNama(value);
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'You must fill this box!';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    obscureText: false,
                    decoration: new InputDecoration(
                      labelText: "Favorite Music Genre",
                      icon: Icon(Icons.music_note),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    onChanged: (String value) {
                      updateHobi(value);
                    },
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'You must fill this box!';
                      }
                      return null;
                    },
                  ),
                ),
                IconButton(
                    icon: Icon(Icons.accessibility),
                    iconSize: 50,
                    color: Colors.black,
                    onPressed: () {
                      if (adaNama == true && adaHobi == true) {
                        setState(() {
                          gabungan = _nama + "-" + _hobi;
                        });
                      } else {
                        setState(() {
                          gabungan = "Belum ada input!";
                        });
                      }
                      ;
                    }),
                Text(
                  gabungan,
                )
              ]),
            ),
          ),
        ));
  }
}
