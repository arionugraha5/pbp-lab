from django.urls import path
from .views import index, friend_list

urlpatterns = [
    path('friends', friend_list, name='friend_list'),
    path('', index, name='index'),
]
