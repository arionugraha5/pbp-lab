from django.shortcuts import render
from .forms import FriendForm
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect


@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values() 
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    context = {}

    # create form object
    form = FriendForm(request.POST or None)

    # check if form data is valid
    if (form.is_valid() and request.method == 'POST'):
        form.save() # save form data to model
        return HttpResponseRedirect('/lab-3/')

    context['form'] = form
    return render(request, "lab3_form.html", context)
