1. Apakah perbedaan antara JSON dan XML?
   JSON (JavaScript Object Notation) adalah sebuah format yang digunakan untuk menyimpan dan mengirim data. JSON berbasis JavaScript dan mudah untuk dipahami karena sifatnya yang self-describing. XML (eXtensible Markup Language) adalah bahasa markup yang digunakan untuk menyimpan dan mengirim data juga. Sifat dari XML juga self-describing. Tujuan utama dari XML adalah untuk membawa data, bukan menunjukkan data. Walaupun keduanya memiliki fungsi utama yang sama, terdapat beberapa perbedaan antara keduanya. Berikut adalah beberapa perbedaanya.
   - JSON menggunakan notasi berdasarkan JavaScript, sedangkan XML menggunakan struktur tag.
   - JSON bisa menyimpan array, XML tidak.
   - XML bisa diberi komen, JSON tidak.
   - JSON hanya support encoding UTF-8, sedangkan XML support untuk banyak encoding.
   - XML support namespaces, JSON tidak.

2. Apakah perbedaan antara HTML dan XML?
   HTML adalah markup language yang didesain dengan tujuan utama untuk menampilkan data sehingga lebih fokus ke cara penampilan data tersebut. Sedangkan XML juga merupakan sebuah markup language, namun XML didesain untuk membawa data yang berarti fokus ke data yang dibawa. Untuk segi struktur, tag yang ada di HTML sudah ditentukan dan memiliki fungsi tertentu pada setiap tagnya. Sedangkan tag di XML tidak ditentukan sebelumnya, tag di XML dibuat sendiri oleh user yang membuat file XML untuk mendefinisikan data yang dibawa oleh file XML tersebut.